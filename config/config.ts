export type DbOptions = {
    username: string;
    password: string;
    database: string;
    host: string;
    port: number;
    dialect: string;
}
export const dbconfig = {
    username: 'admin',
    password: 'admin',
    database: 'articles',
    host: 'localhost',
    port: 15436,
    dialect: 'postgres'
}