import { dbconfig } from './config';
import { Sequelize } from 'sequelize-typescript';
import { Articles } from '../models/Articles';

export const sequelize = new Sequelize({
    dialect: dbconfig.dialect,
    operatorsAliases: Sequelize.Op as any,
    database: dbconfig.database,
    username: dbconfig.username,
    password: dbconfig.password,
    host: dbconfig.host,
    port: dbconfig.port,
    modelPaths: [__dirname + '/models']
});

sequelize.addModels([Articles]);




