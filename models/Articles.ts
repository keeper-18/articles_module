import {
    Table, DataType,
    Column,
    Model,
    CreatedAt,
    UpdatedAt, Default
} from 'sequelize-typescript';

@Table({
    timestamps: true
})

export class Articles extends Model<Articles> {

    constructor(userId: number,
                title:string,
                language: string,
                articleText: string,
                categoryId: number,
                tagList: string[]) {
        super();
        this.userId = userId;
        this.title = title;
        this.language = language;
        this.articleText = articleText;
        this.categoryId = categoryId;
        this.tagList = tagList;
    }

    @Column({
        primaryKey: true,
        autoIncrement: true
    })
    id: number;

    @Column
    userId: number;

    @Column
    title: string;

    @Column
    language: string;

    @Column
    articleText: string;

    @Column
    categoryId: number;

    @Column(DataType.ARRAY(DataType.STRING))
    tagList: string[];

    @CreatedAt
    @Column
    createdAt: Date;

    @UpdatedAt
    @Column
    updatedAt: Date;
}

