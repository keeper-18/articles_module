import { Router, Response, Request } from 'express';
import { Articles } from '../../../models/Articles'
const router: Router = Router();

router.get('/:key', async (req:Request, res:Response) => {

    res.send('result')
});

router.get('/', (req:Request, res:Response) => {
    console.log('Article page');
    res.send('Article page')
});

router.post('/add', async (req:Request, res:Response) => {
    console.log('Adding article...');

    try{
        const tagList = req.body.tagList.split(" "),
            article = new Articles(
            req.body.userId,
            req.body.title,
            req.body.language,
            req.body.articleText,
            parseInt(req.body.categoryId),
            tagList
        );

        const creationResult = await article.save();
        console.log('Finished adding article...', creationResult.dataValues);
    } catch(e) {
        console.log('Error', e);
    }

    res.send('got it!');
    // res.status(200).json(result);
});

export const articlesRoutes: Router = router;