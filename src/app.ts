import express, {Response, Request, RequestHandler} from 'express';
import { sequelize } from '../config/dbconnect'
import * as bodyParser from 'body-parser';
import { articlesRoutes } from './routes/articles';
import cors = require('cors');
const hostname = 'localhost',
      port: number | string = process.env.PORT || 4040,
      app: express.Application = express();

sequelize.authenticate().then(() => {
    console.log("Connected to DB");
})
    .catch((err) => {
        console.log(err);
    })


app.set('port', process.env.PORT || port );
/// <reference types="node" />
// API Endpoints

// middleware for parsing application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({extended: true}));

// middleware for json body parsing
app.use(bodyParser.json({limit: '5mb'}));

app.use(cors({
    methods: ['GET', 'POST', 'PUT', 'DELETE'],
    exposedHeaders: ['Content-Range', 'X-Content-Range'],
    allowedHeaders: ['Content-Type', 'Authorization'],
    origin: "*"
}));

app.use('/article', articlesRoutes);

app.get('/', (req: Request, res: Response) => {
    res.send('Hi! It\'s working...')

});


export default app;